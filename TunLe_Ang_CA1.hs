import Data.List(intersect)
--q1
add_and_double :: (Num a) => a -> a -> a
add_and_double x y = (x+y)*2

--q2
(+*) :: (Num a) => a -> a ->a
(+*) a b = a `add_and_double`b

--q3
solve_quadratic_equation::(Float,Float,Float)->(Float,Float)
solve_quadratic_equation (a,b,c) = (x1,x2)
   where        
      x1 = (-b + sd)/(2*a)
      x2 = (-b - sd)/(2*a)	 
      d = b**2 -4*a*c
      sd = sqrt d 	   
--q4
first_n::(Enum a,Num a)=>Int->[a]
first_n x = take x [1..]


	  
--q6--
product_factorial::(Integral a)=>a->a
product_factorial n = n*factorial(product[1..n-1])
   where
      factorial 0 =1
      factorial n =n*factorial(n-1)

--q7
factorials = 1 : zipWith (*) factorials [1..]
--q8
isPrime::Integer->Bool
isPrime n = length[x|x<-[2..n],n`mod`x==0]==1
--q9
primes::[Integer]
primes = [x|x<-[1..],isPrime x ==True]
--q10
list_sum::[Integer]->Integer
list_sum [] = 0
list_sum (x:xs)=x + list_sum xs

--q11
sum'::(Integral a)=>a->[a]->a
sum' n []=n
sum' n (x:xs)=n+(x + sum' n xs)

--q12
multiply'::(Integral a)=>a->[a]->a
multiply' n [] = 1
multiply' n (x:xs)=n*(x * multiply' n xs)

--q13
guess::(Eq a,Num a)=>String->a->String
guess _ 5 = "Lost"
guess "I Love Functional Programming" 1 ="Win"
guess "I Love Functional Programming" 2 ="Win"
guess "I Love Functional Programming" 3 ="Win"
guess "I Love Functional Programming" 4 ="Win"

--q14
dot_product::(Num a)=>[a]->[a]->a
dot_product xs ys = sum $ zipWith(*) xs ys

--q15
is_even::(Ord a,Eq a,Integral a,Num a)=>a->Bool
is_even n =
   if n`mod`2 ==0
      then True
      else False

--q16
unixname::[String]->String
unixname []=error"empty list"
unixname (x:xs)
	|x =="a" ||x =="e" ||x =="i" ||x =="o" ||x =="u" =" "
	|otherwise = x
--q17
intersection::[Integer]->[Integer]->[Integer]
intersection [] _ =[]
intersection _ [] =[]
intersection xs ys = xs `intersect` ys

--q18
unixname'::[String]->String
unixname' []=error"empty list"
unixname' (x:xs)
	|x =="a" ||x =="e" ||x =="i" ||x =="o" ||x =="u" ="X"
	|otherwise = x
